#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#if false

#include "../DRefTestUtils.hpp"
#include "../DTestClassesHierarchy.hpp"

#include "demiurg_compiletime_reflection/IsClassHasMethod.hpp"
#include "demiurg_compiletime_containers/Optional.hpp"
#include "demiurg_compiletime_flowcontrol/DummyExec.hpp"
#include "demiurg_compiletime_flowcontrol/ExecCombinations.hpp"
#include "demiurg_ref/DNew.hpp"
#include "demiurg_ref/DOwningRef.hpp"
#include "demiurg_ref/DRef.hpp"

template<typename T_Type> constexpr T_Type invalid();

namespace DRefTests{ namespace Equality{

enum class EEqualityResult {
    NotPossible,
    Equals,
    NotEquals,
    NotNullableRefsNulledException,
    Inconsistent,
    Unknown
};

inline const char* toString(const EEqualityResult inEEqualityResult) {
    switch(inEEqualityResult) {
        case EEqualityResult::NotPossible:                      return "EEqualityResult::NotPossible";
        case EEqualityResult::Equals:                           return "EEqualityResult::Equals";
        case EEqualityResult::NotEquals:                        return "EEqualityResult::NotEquals";
        case EEqualityResult::NotNullableRefsNulledException:   return "EEqualityResult::NotNullableRefsNulledException";
        case EEqualityResult::Unknown:                          return "EEqualityResult::Unknown";
        case EEqualityResult::Inconsistent:                     return "EEqualityResult::Inconsistent";
        default:                                                return "<unregisted> EEqualityResult";
    }
}

static constexpr DOptional<EEqualityResult> TrueResult{ };
static constexpr DOptional<EEqualityResult> FalseResult{ EEqualityResult::NotPossible };
static constexpr DOptional<EEqualityResult> InconsistentResult{ EEqualityResult::Inconsistent };

//TODO: Ensure here that top elements in both type Storages has same SEGID? Check if the Storages for all types are empty here?
//Maybe it's good idea to create one universal check for all tests that will ensure that Storages related global state
// still same as before test

struct ERefValidity { struct Valid; struct Uninitialized; struct Freed; };

template<typename T_Type> const char* typeStringName();

template<> const char* typeStringName<ERefValidity::Valid>() { return "ERefValidity::Valid"; }
template<> const char* typeStringName<ERefValidity::Uninitialized>() { return "ERefValidity::Uninitialized"; }
template<> const char* typeStringName<ERefValidity::Freed>() { return "ERefValidity::Freed"; }

struct TestExec {
private:
    template<typename T_LeftType, typename T_LeftAttributes, typename TE_LeftRefValidity,
             typename T_RightType, typename T_RightAttributes, typename TE_RightRefValidity>
    struct Impl;

	//Validity-based combinations
	
    //X..
    //...
    //...
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Valid, T_RightType, T_RightAttributes, ERefValidity::Valid>
    {
    private:
        static EEqualityResult callWith_sameSEGID_sameAddress() {
            DOwningRef<D> theOwningRef{ DNew<D>() };

            DRef<T_LeftType, T_LeftAttributes> theLeftRef{ theOwningRef };
            DRef<T_RightType, T_RightAttributes> theRightRef{ theOwningRef };
            return (theLeftRef == theRightRef) ? EEqualityResult::Equals : EEqualityResult::NotEquals;
        }

        static EEqualityResult callWith_otherSEGID_sameAddress() {
            DOwningRef<D> theOwningRef{ DNew<D>() };

            DRef<T_LeftType, T_LeftAttributes> theLeftRef{ theOwningRef };
            try {
                theOwningRef = DNew<D>();
            } catch(Check::NotNullableRefsLeftOnObjectDestroyException inException) {
                return EEqualityResult::NotNullableRefsNulledException;
            }
            DRef<T_RightType, T_RightAttributes> theRightRef{ theOwningRef };
            return (theLeftRef == theRightRef) ? EEqualityResult::Equals : EEqualityResult::NotEquals;
        }

        static EEqualityResult callWith_sameSEGID_otherAddress() {
            DOwningRef<D> theOwningRefForLeftRef{ DNew<D>() };
            DOwningRef<E> theOwningRefForRightRef{ DNew<E>() };

            DRef<T_LeftType, T_LeftAttributes> theLeftRef{ theOwningRefForLeftRef };
            DRef<T_RightType, T_RightAttributes> theRightRef{ theOwningRefForRightRef };
            return (theLeftRef == theRightRef) ? EEqualityResult::Equals : EEqualityResult::NotEquals;
        }

        static EEqualityResult callWith_otherSEGID_otherAddress() {
            DOwningRef<D> theOwningRefForLeftRef{ DNew<D>() };
            DRef<T_LeftType, T_LeftAttributes> theLeftRef{ theOwningRefForLeftRef };

            DOwningRef<D> theOwningRefForRightRef{ DNew<D>() };
            DRef<T_RightType, T_RightAttributes> theRightRef{ theOwningRefForRightRef };

            return (theLeftRef == theRightRef) ? EEqualityResult::Equals : EEqualityResult::NotEquals;
        }

    public:
        static void _() {
            callWith_sameSEGID_sameAddress();
            callWith_otherSEGID_sameAddress();
            callWith_sameSEGID_otherAddress();
            callWith_otherSEGID_otherAddress();
        }
    };

    //0X.
    //X..
    //...
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Valid, T_RightType, T_RightAttributes, ERefValidity::Uninitialized>
    {
    private:
        struct CheckedCall
        {
            template<typename T_TypeForValid, typename T_AttributesForValid, typename T_TypeForInvalid, typename T_AttributesForInvalid>
            static constexpr const DOptional<EEqualityResult>& check() {
                constexpr bool kIsInvalidRefHasDefaultConstructor = IsClassHasConstructorWithSignature<
                        DRef<T_TypeForInvalid, T_AttributesForInvalid>>::_();
                return ConstexprConditionalRetExec<kIsInvalidRefHasDefaultConstructor,
                        DOptional<EEqualityResult>, TrueResult, FalseResult>::_();
            }

            template<typename T_TypeForValid, typename T_AttributesForValid, typename T_TypeForInvalid, typename T_AttributesForInvalid>
            static EEqualityResult execute() {
                DOwningRef<D> theOwningRefForValidRef{ DNew<D>() };
                DRef<T_TypeForValid, T_AttributesForValid> theValidRef{ theOwningRefForValidRef };

                DRef<T_TypeForInvalid, T_AttributesForInvalid> theInvalidRef;

                const bool theValidRefIsLeftResult = (theValidRef == theInvalidRef);
                const bool theValidRefIsRightResult = (theInvalidRef == theValidRef);
                return (theValidRefIsLeftResult == theValidRefIsRightResult) ?
                        (theValidRefIsLeftResult ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                        EEqualityResult::Inconsistent;
            }
        };

    public:
        static void _() {
            const EEqualityResult theValidAndUninitializedRefsResult = PerformCheckedExec<CheckedCall,
                    T_LeftType, T_LeftAttributes, T_RightType, T_RightAttributes>::_();

            //<<<<<<<<<<<<< TODO: Print result
        }
    };

    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Uninitialized, T_RightType, T_RightAttributes, ERefValidity::Valid>
            : public DummyExec{ };

    //00X
    //0..
    //X..
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Valid, T_RightType, T_RightAttributes, ERefValidity::Freed>
    {
    private:
        using TypeForValid = T_LeftType;
        using AttributesForValid = T_LeftAttributes;

        using TypeForFreed = T_RightType;
        using AttributesForFreed = T_RightAttributes;

    public:
        static void _() {
            EEqualityResult theResult;

            DOwningRef<D> theOwningRefForValidRef{ DNew<D>() };
            DRef<TypeForValid, AttributesForValid> theValidRef{ theOwningRefForValidRef };

            DOwningRef<D> theOwningRefForFreedRef{ DNew<D>() };
            DRef<TypeForFreed, AttributesForFreed> theFreedRef{ theOwningRefForFreedRef };
            try {
                theOwningRefForFreedRef.free();
            } catch(Check::NotNullableRefsLeftOnObjectDestroyException inException) {
                theResult = EEqualityResult::NotNullableRefsNulledException;
            }

            const bool theValidRefIsLeftResult = (theValidRef == theFreedRef);
            const bool theValidRefIsRightResult = (theFreedRef == theValidRef);
            theResult = (theValidRefIsLeftResult == theValidRefIsRightResult) ?
                    (theValidRefIsLeftResult ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                    EEqualityResult::Inconsistent;
        }
    };

    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Freed, T_RightType, T_RightAttributes, ERefValidity::Valid>
            : public DummyExec{ };

    //000
    //0X.
    //0..
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Uninitialized, T_RightType, T_RightAttributes, ERefValidity::Uninitialized>
    {
    private:
        struct CheckedCall
        {
            template<typename T_TypeA, typename T_AttributesA, typename T_TypeB, typename T_AttributesB>
            static constexpr const DOptional<EEqualityResult>& check() {
                constexpr bool kIsRefAHasDefaultConstructor = IsClassHasConstructorWithSignature<DRef<T_TypeA, T_AttributesA>>::_();
                constexpr bool kIsRefBHasDefaultConstructor = IsClassHasConstructorWithSignature<DRef<T_TypeB, T_AttributesB>>::_();
                return ConstexprConditionalRetExec<kIsRefAHasDefaultConstructor && kIsRefBHasDefaultConstructor,
                        DOptional<EEqualityResult>, TrueResult, FalseResult>::_();
            }

            template<typename T_TypeA, typename T_AttributesA, typename T_TypeB, typename T_AttributesB>
            static EEqualityResult execute() {
                DRef<T_TypeA, T_AttributesA> theRefA;

                DRef<T_TypeB, T_AttributesB> theRefB;

                const bool theResultAFirst = (theRefA == theRefB);
                const bool theResultBFirst = (theRefB == theRefA);
                return (theResultAFirst == theResultBFirst) ?
                        (theResultAFirst ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                        EEqualityResult::Inconsistent;
            }
        };

    public:
        static void _() {
            const EEqualityResult theValidAndUninitializedRefsResult = PerformCheckedExec<CheckedCall,
                    T_LeftType, T_LeftAttributes, T_RightType, T_RightAttributes>::_();

            //<<<<<<<<<<<<< TODO: Print result
        }
    };

    //000
    //00X
    //0X.
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Uninitialized, T_RightType, T_RightAttributes, ERefValidity::Freed>
    {
    private:
        struct CheckedCall
        {
            template<typename T_TypeForUninitialized, typename T_AttributesForUninitialized,
                    typename T_TypeForFreed, typename T_AttributesForFreed>
            static constexpr const DOptional<EEqualityResult>& check() {
                constexpr bool kIsRefForUninitializedHasDefaultConstructor =
                        IsClassHasConstructorWithSignature<DRef<T_TypeForUninitialized, T_AttributesForUninitialized>>::_();
                return ConstexprConditionalRetExec<kIsRefForUninitializedHasDefaultConstructor,
                        DOptional<EEqualityResult>, TrueResult, FalseResult>::_();
            }

            template<typename T_TypeForUninitialized, typename T_AttributesForUninitialized,
                    typename T_TypeForFreed, typename T_AttributesForFreed>
            static EEqualityResult execute() {
                DRef<T_TypeForUninitialized, T_AttributesForUninitialized> theUninitializedRef;

                DOwningRef<D> theOwningRefForFreedRef{ DNew<D>() };
                DRef<T_TypeForFreed, T_AttributesForFreed> theFreedRef{ theOwningRefForFreedRef };
                try {
                    theOwningRefForFreedRef.free();
                } catch(Check::NotNullableRefsLeftOnObjectDestroyException inException) {
                    return EEqualityResult::NotNullableRefsNulledException;
                }

                const bool theResultForOwningFirst = (theOwningRefForFreedRef == theFreedRef);
                const bool theResultForFreedFirst = (theFreedRef == theOwningRefForFreedRef);
                return (theResultForOwningFirst == theResultForFreedFirst) ?
                        (theResultForOwningFirst ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                        EEqualityResult::Inconsistent;
            }
        };

    public:
        static void _() {
            const EEqualityResult theValidAndUninitializedRefsResult = PerformCheckedExec<CheckedCall,
                    T_LeftType, T_LeftAttributes, T_RightType, T_RightAttributes>::_();

            //<<<<<<<<<<<<< TODO: Print result
        }
    };

    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Freed, T_RightType, T_RightAttributes, ERefValidity::Uninitialized>
            : public DummyExec{ };

    //000
    //000
    //00X
    template<typename T_LeftType, typename T_LeftAttributes, typename T_RightType, typename T_RightAttributes>
    struct Impl<T_LeftType, T_LeftAttributes, ERefValidity::Freed, T_RightType, T_RightAttributes, ERefValidity::Freed>
    {
    private:
        using TypeA = T_LeftType;
        using AttributesA = T_LeftAttributes;

        using TypeB = T_RightType;
        using AttributesB = T_RightAttributes;

        struct SameFreedRefsEqualityTestExec
        {
            static EEqualityResult _() {
                DOwningRef<D> theOwningRef{ DNew<D>() };
                DRef<TypeA, AttributesA> theRefA{ theOwningRef };
                DRef<TypeB, AttributesB> theRefB{ theOwningRef };

                try {
                    theOwningRef.free();
                } catch(Check::NotNullableRefsLeftOnObjectDestroyException inException) {
                    return EEqualityResult::NotNullableRefsNulledException;
                }

                const bool theResultAFirst = (theRefA == theRefB);
                const bool theResultBFirst = (theRefB == theRefA);
                return (theResultAFirst == theResultBFirst) ?
                        (theResultAFirst ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                        EEqualityResult::Inconsistent;
            }
        };

        struct DifferentFreedRefsEqualityTestExec
        {
            static EEqualityResult _() {
                DOwningRef<D> theOwningRefA{ DNew<D>() };
                DOwningRef<D> theOwningRefB{ DNew<D>() };

                DRef<TypeA, AttributesA> theRefA{ theOwningRefA };
                DRef<TypeB, AttributesB> theRefB{ theOwningRefB };

                try {
                    theOwningRefA.free();
                    theOwningRefB.free();
                } catch(Check::NotNullableRefsLeftOnObjectDestroyException inException) {
                    return EEqualityResult::NotNullableRefsNulledException;
                }

                const bool theResultAFirst = (theRefA == theRefB);
                const bool theResultBFirst = (theRefB == theRefA);
                return (theResultAFirst == theResultBFirst) ?
                        (theResultAFirst ? EEqualityResult::Equals : EEqualityResult::NotEquals) :
                        EEqualityResult::Inconsistent;
            }
        };

    public:
        static void _() {
            SameFreedRefsEqualityTestExec::_();
            DifferentFreedRefsEqualityTestExec::_();
        }
    };

public:
    static void _() {
        using AllTypes = TypesPack<A, D>;
        using AllAttributes = TypesPack<CCN, MCN, CMN, MMN, CC, MC, CM, MM>;
        using AllValidities = TypesPack<ERefValidity::Valid, ERefValidity::Uninitialized, ERefValidity::Freed>;
        ExecCombinations<Impl, AllTypes, AllAttributes, AllValidities, AllTypes, AllAttributes, AllValidities>::_();
    }
};

}}// namespace DRefTests::Equality

#endif

#endif
