#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#include "utils/Types.hpp"
#include "utils/CTRMeta.hpp"

#include "demiurg_ref/DRef.hpp"
#include "demiurg_ref/DOwningRef.hpp"
#include "demiurg_compiletime_reflection/typeInfo/GenTypeInfo.hpp"
#include "demiurg_typescore/TypesPack.hpp"
#include "demiurg_compiletime_flowcontrol/ExecCombinations.hpp"

namespace DRefTests{ namespace Tests{ namespace _API{

    template<typename T_APIMutability, typename T_RefMutability, typename T_Nullability>
    struct TestExec
    {
        static constexpr void _() {
            _1_checkDefaultConstructing();
            _2_checkAssigningAndMovingConstruction();
            _3_checkAssigningAndMovingOperators();
            _4_checkDereferencing();
            _5_checkValidityChecking();
            _6_checkInvalidation();
        }

        static constexpr void _1_checkDefaultConstructing()
        {
            using Class = DRefTests::TypesForTesting::Class;
            using Attributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
            using RefType = DRef<Class, Attributes>;
            using TypeInfo = CTR::GenTypeInfo<RefType>;

            constexpr bool hasDefaultConstructor = TypeInfo::isDefaultConstructorMayBeCalled();

            constexpr bool isNullableCheckPassed = hasDefaultConstructor;
            constexpr bool isNotNullableCheckPassed = !hasDefaultConstructor;
            constexpr bool isCheckPassed = Attributes::isNullable() ? isNullableCheckPassed: isNotNullableCheckPassed;

            static_assert(isCheckPassed, "Default constructing check not passed");
        }

        static constexpr void _2_checkAssigningAndMovingConstruction() {
            _2_checkAssigningConstruction_owningRef();
            _2_checkAssigningAndMovingConstruction_ref();
        }

        template<typename T_RightRefType>
        struct Exec_2_checkAssigningConstruction_owningRef
        {
            constexpr static void _()
            {
                using LeftClass = DRefTests::TypesForTesting::Class;
                using LeftAttributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
                using LeftRefType = DRef<LeftClass, LeftAttributes>;
                using LeftTypeInfo = CTR::GenTypeInfo<LeftRefType>;

                using RightRefType = DOwningRef<T_RightRefType>;

                constexpr bool hasAssignConstructor = LeftTypeInfo::template
                        isConstructorMayBeCalled<const RightRefType&>();

                constexpr bool areTypesMayBeAssigned = isAAssignableFromB<LeftClass*, T_RightRefType*>();

                constexpr bool isCheckPassed = areTypesMayBeAssigned ? hasAssignConstructor : !hasAssignConstructor;
                static_assert(isCheckPassed, "Owning ref assigning and moving construction check not passed");
            }
        };

        static constexpr void _2_checkAssigningConstruction_owningRef() {
            using Execs = typename GenCombinations<
                Exec_2_checkAssigningConstruction_owningRef,
                DRefTests::TypesForTesting::Assignment::RightTypesPack>::_;
            ExecAll<Execs>::_();
        }

        // ==================================================={{{
        template<typename T_RightRefType,
                 typename T_RightRefAPIMutability,
                 typename T_RightRefRefMutability,
                 typename T_RightRefNullability>
        struct Exec_2_checkAssigningAndMovingConstruction_ref
        {
            constexpr static void _()
            {
                using LeftClass = DRefTests::TypesForTesting::Class;
                using LeftAttributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
                using LeftRefType = DRef<LeftClass, LeftAttributes>;
                using LeftTypeInfo = CTR::GenTypeInfo<LeftRefType>;

                using RightAttributes = DRefAttributes<T_RightRefAPIMutability, T_RightRefRefMutability, T_RightRefNullability>;
                using RightRefType = DRef<T_RightRefType, RightAttributes>;

                constexpr bool hasAssignConstructor = LeftTypeInfo::template
                        isConstructorMayBeCalled<const RightRefType&>();
                constexpr bool hasMoveConstructor = LeftTypeInfo::template
                        isConstructorMayBeCalled<RightRefType&&>();

                constexpr bool isTypesMayBeAssignedCheckPassed = isAAssignableFromB<LeftClass*, T_RightRefType*>();
                constexpr bool isConstAPICheckPassed =
                        LeftAttributes::isConstAPI() ||
                        (LeftAttributes::isMutableAPI() && RightAttributes::isMutableAPI());
                constexpr bool isCommonChecksPassed =
                        isTypesMayBeAssignedCheckPassed &&
                        isConstAPICheckPassed;

                constexpr bool isAssignmentShouldExist = isCommonChecksPassed;
                constexpr bool isAssignmentCheckPassed = isAssignmentShouldExist ? hasAssignConstructor : !hasAssignConstructor;
                static_assert(isAssignmentCheckPassed, "Assignment check not passed");

                constexpr bool isRightMayBeMoved = RightAttributes::isMutableRef() && RightAttributes::isNullable();
                constexpr bool isMovingShouldExist = isCommonChecksPassed && isRightMayBeMoved;

                constexpr bool isMovingCheckPassed = isMovingShouldExist ? hasMoveConstructor : !hasMoveConstructor;
                static_assert(isMovingCheckPassed, "Moving check not passed");
            }
        };

        template<typename _Type>
        using TypeSubstitutionConstructionExec = Exec_2_checkAssigningAndMovingConstruction_ref<
            _Type, T_APIMutability, T_RefMutability, T_Nullability>;

        template<typename _APIMutailibty>
        using APIMutailibtySubstitutionConstructionExec = Exec_2_checkAssigningAndMovingConstruction_ref<
            DRefTests::TypesForTesting::Class, _APIMutailibty, T_RefMutability, T_Nullability>;

        template<typename _RefMutailibty>
        using RefMutailibtySubstitutionConstructionExec = Exec_2_checkAssigningAndMovingConstruction_ref<
            DRefTests::TypesForTesting::Class, T_APIMutability, _RefMutailibty, T_Nullability>;

        template<typename _Nullability>
        using NullabilitySubstitutionConstructionExec = Exec_2_checkAssigningAndMovingConstruction_ref<
            DRefTests::TypesForTesting::Class, T_APIMutability, T_RefMutability, _Nullability>;

        static constexpr void _2_checkAssigningAndMovingConstruction_ref() {
            using TypeSubstitutionExecs = typename GenCombinations<
                TypeSubstitutionConstructionExec,
                DRefTests::TypesForTesting::Assignment::RightTypesPack>::_;
            ExecAll<TypeSubstitutionExecs>::_;

            using APIMutailibtySubstitutionExecs = typename GenCombinations<
                APIMutailibtySubstitutionConstructionExec,
                DRefTests::TypesForTesting::Assignment::RightAPIMutabilities>::_;
            ExecAll<APIMutailibtySubstitutionExecs>::_();

            using RefMutailibtySubstitutionExecs = typename GenCombinations<
                RefMutailibtySubstitutionConstructionExec,
                DRefTests::TypesForTesting::Assignment::RightRefMutabilities>::_;
            ExecAll<RefMutailibtySubstitutionExecs>::_();

            using NullabilitySubstitutionExecs = typename GenCombinations<
                NullabilitySubstitutionConstructionExec,
                DRefTests::TypesForTesting::Assignment::RightNullabilities>::_;
            ExecAll<NullabilitySubstitutionExecs>::_();
        }
        // ===================================================}}}

        static constexpr void _3_checkAssigningAndMovingOperators() {
            _3_checkAssigningOperator_owningRef();
            _3_checkAssigningAndMovingOperator_ref();
        }

        template<typename T_RightRefType>
        struct Exec_3_checkAssigningOperator_owningRef
        {
            constexpr static void _()
            {
                using LeftClass = DRefTests::TypesForTesting::Class;
                using LeftAttributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
                using LeftRefType = DRef<LeftClass, LeftAttributes>;
                using LeftTypeInfo = CTR::GenTypeInfo<LeftRefType>;
                using RightRefType = DOwningRef<T_RightRefType>;

                using namespace DRefTests::CTRMeta;

                constexpr bool hasAssignOperator = LeftTypeInfo::template
                        isMethodExist<AssignmentOperator, LeftRefType&(const RightRefType&), CTR::NonConst>();

                constexpr bool areTypesMayBeAssigned = isAAssignableFromB<LeftClass*, T_RightRefType*>();

                constexpr bool isConstRefCheckPassed = !hasAssignOperator;
                constexpr bool isMutableRefCheckPassed = areTypesMayBeAssigned ? hasAssignOperator : !hasAssignOperator;

                constexpr bool isCheckPassed = LeftAttributes::isConstRef() ? isConstRefCheckPassed : isMutableRefCheckPassed;
                static_assert(isCheckPassed, "Owning ref assigning and moving construction check not passed");
            }
        };

        static constexpr void _3_checkAssigningOperator_owningRef() {
            using Execs = typename GenCombinations<
                Exec_3_checkAssigningOperator_owningRef,
                DRefTests::TypesForTesting::Assignment::RightTypesPack>::_;
            ExecAll<Execs>::_();
        }

        // ==================================================={{{
        template<typename T_RightRefType,
                 typename T_RightRefAPIMutability,
                 typename T_RightRefRefMutability,
                 typename T_RightRefNullability>
        struct Exec_3_checkAssigningAndMovingOperator_ref
        {
            constexpr static void _()
            {
                using LeftClass = DRefTests::TypesForTesting::Class;
                using LeftAttributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
                using LeftRefType = DRef<LeftClass, LeftAttributes>;
                using LeftTypeInfo = CTR::GenTypeInfo<LeftRefType>;

                using RightAttributes = DRefAttributes<T_RightRefAPIMutability, T_RightRefRefMutability, T_RightRefNullability>;
                using RightRefType = DRef<T_RightRefType, RightAttributes>;

                using namespace DRefTests::CTRMeta;

                constexpr bool hasAssignOperator = LeftTypeInfo::template
                        isMethodExist<AssignmentOperator, LeftRefType&(const RightRefType&), CTR::NonConst>();
                constexpr bool hasMoveOperator = LeftTypeInfo::template
                        isMethodExist<AssignmentOperator, LeftRefType&(RightRefType&&), CTR::NonConst>();

                constexpr bool isTypesMayBeAssignedCheckPassed = isAAssignableFromB<LeftClass*, T_RightRefType*>();
                constexpr bool isConstAPICheckPassed =
                        LeftAttributes::isConstAPI() ||
                        (LeftAttributes::isMutableAPI() && RightAttributes::isMutableAPI());
                constexpr bool isConstRefCheckPassed = LeftAttributes::isMutableRef();
                constexpr bool isCommonChecksPassed =
                        isTypesMayBeAssignedCheckPassed &&
                        isConstAPICheckPassed &&
                        isConstRefCheckPassed;

                constexpr bool isAssignmentShouldExist = isCommonChecksPassed;
                constexpr bool isAssignmentCheckPassed = isAssignmentShouldExist ? hasAssignOperator : !hasAssignOperator;
                static_assert(isAssignmentCheckPassed, "Assignment check not passed");

                constexpr bool isRightMayBeMoved = RightAttributes::isMutableRef() && RightAttributes::isNullable();
                constexpr bool isMovingShouldExist = isCommonChecksPassed && isRightMayBeMoved;
                constexpr bool isMovingCheckPassed = isMovingShouldExist ? hasMoveOperator : !hasMoveOperator;
                static_assert(isMovingCheckPassed, "Moving check not passed");
            }
        };

        template<typename _Type>
        using TypeSubstitutionOperatorExec = Exec_3_checkAssigningAndMovingOperator_ref<
            _Type, T_APIMutability, T_RefMutability, T_Nullability>;

        template<typename _APIMutailibty>
        using APIMutailibtySubstitutionOperatorExec = Exec_3_checkAssigningAndMovingOperator_ref<
            DRefTests::TypesForTesting::Class, _APIMutailibty, T_RefMutability, T_Nullability>;

        template<typename _RefMutailibty>
        using RefMutailibtySubstitutionOperatorExec = Exec_3_checkAssigningAndMovingOperator_ref<
            DRefTests::TypesForTesting::Class, T_APIMutability, _RefMutailibty, T_Nullability>;

        template<typename _Nullability>
        using NullabilitySubstitutionOperatorExec = Exec_3_checkAssigningAndMovingOperator_ref<
            DRefTests::TypesForTesting::Class, T_APIMutability, T_RefMutability, _Nullability>;

        static constexpr void _3_checkAssigningAndMovingOperator_ref() {
            using TypeSubstitutionExecs = typename GenCombinations<
                TypeSubstitutionOperatorExec,  DRefTests::TypesForTesting::Assignment::RightTypesPack>::_;
            ExecAll<TypeSubstitutionExecs>::_();

            using APIMutailibtySubstitutionExecs = typename GenCombinations<
                APIMutailibtySubstitutionOperatorExec,  DRefTests::TypesForTesting::Assignment::RightAPIMutabilities>::_;
            ExecAll<APIMutailibtySubstitutionExecs>::_();

            using RefMutailibtySubstitutionExecs = typename GenCombinations<
                RefMutailibtySubstitutionOperatorExec,  DRefTests::TypesForTesting::Assignment::RightRefMutabilities>::_;
            ExecAll<RefMutailibtySubstitutionExecs>::_();

            using NullabilitySubstitutionExecs = typename GenCombinations<
                NullabilitySubstitutionOperatorExec,  DRefTests::TypesForTesting::Assignment::RightNullabilities>::_;
            ExecAll<NullabilitySubstitutionExecs>::_();
        }
        // ===================================================}}}

        static constexpr void _4_checkDereferencing()
        {
            using Class = DRefTests::TypesForTesting::Class;
            using Attributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
            using RefType = DRef<Class, Attributes>;
            using TypeInfo = CTR::GenTypeInfo<RefType>;

            using namespace DRefTests::CTRMeta;

            constexpr bool hasArrowOperatorRetMutable = TypeInfo::template
                    isMethodExist<ArrowOperator, Class*(), CTR::Const>();
            constexpr bool hasStarOperatorRetMutable = TypeInfo::template
                    isMethodExist<StarOperator, Class&(), CTR::Const>();
            constexpr bool hasMutableDereferencing = hasArrowOperatorRetMutable && hasStarOperatorRetMutable;

            constexpr bool hasArrowOperatorRetConst = TypeInfo::template
                    isMethodExist<ArrowOperator, const Class*(), CTR::Const>();
            constexpr bool hasStarOperatorRetConst = TypeInfo::template
                    isMethodExist<StarOperator, const Class&(), CTR::Const>();
            constexpr bool hasConstDereferencing = hasArrowOperatorRetConst && hasStarOperatorRetConst;

            constexpr bool isMutableAPICheckPassed = hasMutableDereferencing && !hasConstDereferencing;
            constexpr bool isConstAPICheckPassed = !hasMutableDereferencing && hasConstDereferencing;

            constexpr bool isCheckPassed = Attributes::isMutableAPI() ? isMutableAPICheckPassed : isConstAPICheckPassed;
            static_assert(isCheckPassed, "Check not passed");
        }

        static constexpr void _5_checkValidityChecking()
        {
            using Class = DRefTests::TypesForTesting::Class;
            using Attributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
            using RefType = DRef<Class, Attributes>;
            using TypeInfo = CTR::GenTypeInfo<RefType>;

            using namespace DRefTests::CTRMeta;

            constexpr bool hasIsValidMethod = TypeInfo::template
                    isMethodExist<isValid, bool(), CTR::Const>();

            constexpr bool isNullableCheckPassed = hasIsValidMethod;
            constexpr bool isNotNullableCheckPassed = !hasIsValidMethod;
            constexpr bool isCheckPassed = Attributes::isNullable() ? isNullableCheckPassed: isNotNullableCheckPassed;

            static_assert(isCheckPassed, "Is valid method check not passed");
        }

        static constexpr void _6_checkInvalidation()
        {
            using Class = DRefTests::TypesForTesting::Class;
            using Attributes = DRefAttributes<T_APIMutability, T_RefMutability, T_Nullability>;
            using RefType = DRef<Class, Attributes>;
            using TypeInfo = CTR::GenTypeInfo<RefType>;

            using namespace DRefTests::CTRMeta;

            constexpr bool hasInvalidateMethod = TypeInfo::template isMethodExist<invalidate, void(), CTR::NonConst>();

            constexpr bool isNullableCheckPassed = Attributes::isConstRef() ? !hasInvalidateMethod : hasInvalidateMethod;
            constexpr bool isNotNullableCheckPassed = !hasInvalidateMethod;
            constexpr bool isCheckPassed = Attributes::isNullable() ? isNullableCheckPassed: isNotNullableCheckPassed;

            static_assert(isCheckPassed, "Invalidating method check not passed");
        }
    };

    // ------------------------------------------

    void performTests() {
        using CompileTimeTests = typename GenCombinations<
            TestExec,
            TypesPack<MutableAPI, ConstAPI>,
            TypesPack<MutableRef, ConstRef>,
            TypesPack<Nullable, NotNullable>>::_;
        ExecAll<CompileTimeTests>::_();
    }

}}}//namespace DRefTests::Tests::_API

#endif
