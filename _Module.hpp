#include "../DModuleCore.hpp"
#include "demiurg_ref/_Module.hpp"
#include "wrapped_googletest/_Module.hpp"
#include "demiurg_compiletime_reflection/_Module.hpp"
#include "demiurg_compiletime_containers/_Module.hpp"

#if DMACRO_CheckModuleVersion(demiurg_ref, 1, 1) &&\
    DMACRO_CheckModuleVersion(wrapped_googletest, 1, 1) &&\
    DMACRO_CheckModuleVersion(demiurg_compiletime_reflection, 1, 1) &&\
    DMACRO_CheckModuleVersion(demiurg_compiletime_containers, 1, 1)
#define MODULE__demiurg_ref_tests__Version 1
#endif
