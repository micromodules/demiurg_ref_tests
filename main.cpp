#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#include "tests/_API.h"
#include "tests/_7_Equality.hpp"

int main(int argc, char **argv) {
    DRefTests::Tests::_API::performTests();

    return 0;
}

#endif
