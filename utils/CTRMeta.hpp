#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#include "demiurg_compiletime_reflection/typeInfo/Meta.hpp"

namespace DRefTests{ namespace CTRMeta{

    DDECL_OperatorMeta(ArrowOperator, ->)
    DDECL_OperatorMeta(StarOperator, *)
    DDECL_IdentifierMeta(isValid)
    DDECL_IdentifierMeta(invalidate)
    DDECL_OperatorMeta(AssignmentOperator, =)

}} //DRefTests::CTR::Meta

#endif
