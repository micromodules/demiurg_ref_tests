#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#include "demiurg_ref/DRef.hpp"

template<typename T> constexpr const char* typeStringName();

template<> constexpr const char* typeStringName<CCN>() { return "DRefAttributes<CCN>"; }
template<> constexpr const char* typeStringName<MCN>() { return "DRefAttributes<MCN>"; }
template<> constexpr const char* typeStringName<CMN>() { return "DRefAttributes<CMN>"; }
template<> constexpr const char* typeStringName<MMN>() { return "DRefAttributes<MMN>"; }

template<> constexpr const char* typeStringName<CC>() { return "DRefAttributes<CC>"; }
template<> constexpr const char* typeStringName<MC>() { return "DRefAttributes<MC>"; }
template<> constexpr const char* typeStringName<CM>() { return "DRefAttributes<CM>"; }
template<> constexpr const char* typeStringName<MM>() { return "DRefAttributes<MM>"; }

#endif
