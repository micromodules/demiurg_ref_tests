#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref_tests__Version

#include "demiurg_ref/DRefAttributes.hpp"

namespace DRefTests{ namespace TypesForTesting{

    struct ClassRootBase { char aField{ 1 }; };
    struct ClassFirstBase : public virtual ClassRootBase { char bField{ 2 }; };
    struct ClassSecondBase : public virtual ClassRootBase { double cField[2]{ 3.0, 4.0 }; };
    struct Class : public ClassFirstBase, public ClassSecondBase { short int dField{ 5 }; };
    struct ClassChild : public Class { float eField{ 6.f }; };
    struct ClassNotRelated { long long fField{ 7 }; };

    template<typename T> constexpr const char* typeStringName();

    //template<> constexpr const char* typeStringName<A>() { return "A"; }
    //template<> constexpr const char* typeStringName<B>() { return "B"; }
    //template<> constexpr const char* typeStringName<C>() { return "C"; }
    //template<> constexpr const char* typeStringName<D>() { return "D"; }
    //template<> constexpr const char* typeStringName<E>() { return "E"; }
    //template<> constexpr const char* typeStringName<F>() { return "F"; }

    namespace Assignment
    {
        using LeftTypesPack = TypesPack<
            Class>;

        using RightTypesPack = TypesPack<
            ClassRootBase,
            ClassSecondBase,
            Class,
            ClassChild,
            ClassNotRelated>;

        using RightAPIMutabilities = TypesPack<
            MutableAPI,
            ConstAPI>;

        using RightRefMutabilities = TypesPack<
            MutableRef,
            ConstRef>;

        using RightNullabilities = TypesPack<
            Nullable,
            NotNullable>;
    }

}} //namespace DRefTests::Types

#endif
